package com.ontoweb.uflo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="demo")
public class Demo {
    @Id
    @Column(name="id_",length = 60)
    private String id;
    @Column(name="name_",length = 60)
    private String name;
    @Column(name="desc_",length = 60)
    private String desc;  //描述

    public String getOpion() {
        return opion;
    }

    public void setOpion(String opion) {
        this.opion = opion;
    }

    @Column(name="option_",length = 60)
    private String opion;  //描述

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
