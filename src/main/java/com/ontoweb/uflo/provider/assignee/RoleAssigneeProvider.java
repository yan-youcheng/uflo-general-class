package com.ontoweb.uflo.provider.assignee;

import com.bstek.uflo.env.Context;
import com.bstek.uflo.model.ProcessInstance;
import com.bstek.uflo.process.assign.AssigneeProvider;
import com.bstek.uflo.process.assign.Entity;
import com.bstek.uflo.process.assign.PageQuery;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @Author 16177
 * @Date 2020/11/3 22:02
 * @Description RoleAssigneeProvider
 */
@Component
public class RoleAssigneeProvider implements AssigneeProvider {
    @Override
    public boolean isTree() {
        return false;
    }

    @Override
    public String getName() {
        return "角色列表";
    }

    @Override
    public void queryEntities(PageQuery<Entity> pageQuery, String s) {
        List<Entity> result = new ArrayList<Entity>();
        pageQuery.setRecordCount(100);
        int pageIndex = pageQuery.getPageIndex();
        int pageSize = pageQuery.getPageSize();
        for (int i=(pageIndex-1)*pageSize; i<pageIndex*pageSize; i++) {
            Entity entity = new Entity(String.valueOf(i), "role" + i);
            result.add(entity);
        }
        pageQuery.setResult(result);
    }

    @Override
    public Collection<String> getUsers(String s, Context context, ProcessInstance processInstance) {
        return null;
    }

    @Override
    public boolean disable() {
        return false;
    }
}
