package com.ontoweb.uflo.provider;

import com.bstek.uflo.env.EnvironmentProvider;
import com.ontoweb.uflo.utils.RequestHolder;
import org.hibernate.SessionFactory;
import org.springframework.transaction.PlatformTransactionManager;

import javax.servlet.http.HttpServletRequest;


public class UfloEnvironmentProvider implements EnvironmentProvider {
    private SessionFactory sessionFactory;
    private PlatformTransactionManager platformTransactionManager;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public PlatformTransactionManager getPlatformTransactionManager() {
        return platformTransactionManager;
    }

    public void setPlatformTransactionManager(
            PlatformTransactionManager platformTransactionManager) {
        this.platformTransactionManager = platformTransactionManager;
    }

    public String getCategoryId() {
        return null;
    }

    //通过RequestHolder去取登陆的用户名
    public String getLoginUser() {
        HttpServletRequest req = RequestHolder.getThreadLocal();
        return (String) req.getSession().getAttribute("loginUser");
    }


}
