package com.ontoweb.uflo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * 处理登陆发过来的请求的Controller
 */
@Controller
public class LoginController {
    @RequestMapping("/doLogin")
    @ResponseBody
    public Object doLogin(HttpServletRequest req){
        String username=req.getParameter("username");
        String password=req.getParameter("password");
        if(username==null||password==null){
            throw  new RuntimeException("登陆失败");

        }
        req.getSession().setAttribute("loginUser",username);

        return null;
    }
}
