package com.ontoweb.uflo.filter;

import com.ontoweb.uflo.utils.RequestHolder;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * Filer:把Request中用户名灌入filter中
 */
public class UfloFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req=(HttpServletRequest) request;
        RequestHolder.setThreadLocal(req);
        try{
            chain.doFilter(request,response);
        }finally {
            RequestHolder.remove();
        }
    }

    @Override
    public void destroy() {

    }
}
