package com.ontoweb.uflo.utils;



import javax.servlet.http.HttpServletRequest;

/*
* 保存ThreadLocal的对象
* */
public class RequestHolder {
    private static final ThreadLocal<HttpServletRequest> threadLocal = new ThreadLocal<HttpServletRequest>();

    public static void setThreadLocal(HttpServletRequest request) {
        threadLocal.set(request);
    }

    public static HttpServletRequest getThreadLocal() {
        return threadLocal.get();
    }

    public static void remove() {
        threadLocal.remove();
    }
}
